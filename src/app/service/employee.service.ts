import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Response } from '../models/utils/response.model';
import { Area } from '../models/area.model';
import { Injectable } from '@angular/core';
import { Country } from '../models/country.model';
import { IdentificationType } from '../models/indetificationtype.model';
import { Employee } from '../models/employee.model';

@Injectable({
    providedIn: 'root',
})
  export class EmployeeService {
    private urlApi: string;

    constructor(private http: HttpClient) {
      this.urlApi = environment.urlApi;
    }

    getEmployees(params: object = {}): Observable<Response<Employee>> {
      const headers = new HttpHeaders().set('Content-Type', 'application/json')
      const options = {
        headers,
        params: {
          perPage: "1",
          currentPage: "10",
          ...params
        }
      };
      return this.http.get(`${this.urlApi}empleado/empleados`, options).pipe(
        map((resp: Response<Employee>) => {
          return resp;
        })
      );
    }

    getAreas(): Observable<Response<Area>> {
      const headers = new HttpHeaders().set('Content-Type', 'application/json')
      const options = {
        headers,
      };
      return this.http.get(`${this.urlApi}area/areas`, options).pipe(
        map((resp: Response<Area>) => {
          return resp;
        })
      );
    }

    getCountries(): Observable<Response<Country>> {
      const headers = new HttpHeaders().set('Content-Type', 'application/json')
      const options = {
        headers,
      };
      return this.http.get(`${this.urlApi}pais/paises`, options).pipe(
        map((resp: Response<Country>) => {
          return resp;
        })
      );
    }

    getIdentificationType(): Observable<Response<IdentificationType>> {
      const headers = new HttpHeaders().set('Content-Type', 'application/json')
      const options = {
        headers,
      };
      return this.http.get(`${this.urlApi}tipoIdentificacion/identificaciones`, options).pipe(
        map((resp: Response<IdentificationType>) => {
          return resp;
        })
      );
    }

    postEmployee(employee: Employee): Observable<Response<Employee>> {
      const headers = new HttpHeaders().set('Content-Type', 'application/json')
      const options = {
        headers,
      };
      return this.http.post(`${this.urlApi}empleado/empleado`, employee, options).pipe(
        map((resp: Response<Employee>) => {
          return resp;
        })
      );
    }

    putEmployee(employeeId: number, employee: Employee): Observable<Response<Employee>> {
      const headers = new HttpHeaders().set('Content-Type', 'application/json')
      const options = {
        headers,
      };
      return this.http.put(`${this.urlApi}empleado/empleado/` + employeeId, employee, options).pipe(
        map((resp: Response<Employee>) => {
          return resp;
        })
      );
    }

    deleteEmployee(idEmployee: number): Observable<Response<Employee>> {
      const headers = new HttpHeaders().set('Content-Type', 'application/json')
      const options = {
        headers,
      };
      return this.http.delete(`${this.urlApi}empleado/empleado/` + idEmployee, options).pipe(
        map((resp: Response<Employee>) => {
          return resp;
        })
      );
    }
}