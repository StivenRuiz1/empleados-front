import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
} from "@angular/forms";
import { Router } from "@angular/router";
import { Employee } from "src/app/models/employee.model";
import { Area } from "src/app/models/area.model";
import { Country } from "src/app/models/country.model";
import { IdentificationType } from "src/app/models/indetificationtype.model";
import { Response } from "src/app/models/utils/response.model";
import { EmployeeService } from "src/app/service/employee.service";
import * as moment from "moment";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: "creacion",
  templateUrl: "./creacion.component.html",
  styleUrls: ["./creacion.component.css"],
})
export class CreacionComponent implements OnInit {
  messageResponse: any = "";
  public maxDate: Date = new Date();
  public minDate: Date = moment(new Date()).add(-1, "months").toDate();

  public createEmployee: FormGroup = this.employeeForm.group({
    surname: ["", [Validators.required, Validators.pattern]],
    secondSurname: ["", [Validators.required, Validators.pattern]],
    firstName: ["", [Validators.required, Validators.pattern]],
    otherNames: ["", [Validators.pattern]],
    countryEmployment: ["", [Validators.required]],
    identificationType: [""],
    identificationNumber: ["", [Validators.pattern]],
    email: ["", [Validators.email]],
    admissionDate: [null],
    area: ["", Validators.required],
    state: ["ACTIVO", Validators.required],
    registrationDate: [null],
    modificationDate: [null],
  });

  public areas: Array<Area> = [];
  public countries: Array<Country> = [];
  public identificationTypes: Array<IdentificationType> = [];

  public employeeEdit: Employee = new Employee();

  public isEdit: boolean = false;

  constructor(
    private employeeForm: FormBuilder,
    private employeeService: EmployeeService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}

  get f(): { [key: string]: AbstractControl } {
    return this.createEmployee.controls;
  }

  ngOnInit() {
    this.spinner.show();
    this.chargeInitInfo();
    this.chargeLists();
  }

  chargeInitInfo() {
    let employeeToEdit = localStorage.getItem("employeeToEdit");
    if (employeeToEdit) {
      this.editEmployee(JSON.parse(employeeToEdit));
    } else {
      this.createEmployee.controls["state"].disable();
      let dateTimeNow = moment(new Date()).format().substring(0, 16);
      this.createEmployee.controls["registrationDate"].setValue(dateTimeNow);
    }
    this.createEmployee.controls["registrationDate"].disable();
  }

  chargeLists() {
    this.employeeService
      .getCountries()
      .subscribe((response: Response<Country>) => {
        if (response.data && response.data.length > 0) {
          this.countries = response.data;
          if (this.isEdit) {
            this.createEmployee.controls["countryEmployment"].setValue(
              this.countries.find((country) => {
                return country.countryId ==
                  this.createEmployee.controls["countryEmployment"].value[
                    "countryId"
                  ]
                  ? country
                  : null;
              })
            );
          }
        }
      });

    this.employeeService.getAreas().subscribe((response: Response<Area>) => {
      if (response.data && response.data.length > 0) {
        this.areas = response.data;
        if (this.isEdit) {
          this.createEmployee.controls["area"].setValue(
            this.areas.find((area) => {
              return area.areaId ==
                this.createEmployee.controls["area"].value[
                  "areaId"
                ]
                ? area
                : null;
            })
          );
        }
      }
    });

    this.employeeService
      .getIdentificationType()
      .subscribe((response: Response<IdentificationType>) => {
        if (response.data && response.data.length > 0) {
          this.identificationTypes = response.data;
          if (this.isEdit) {
            this.createEmployee.controls["identificationType"].setValue(
              this.identificationTypes.find((identificationType) => {
                return identificationType.identificationTypeId ==
                  this.createEmployee.controls["identificationType"].value[
                    "identificationTypeId"
                  ]
                  ? identificationType
                  : null;
              })
            );
          }
        }
      });
      this.spinner.hide();
  }

  editEmployee(employee: Employee) {
    this.employeeEdit = employee;
    console.log(this.employeeEdit);
    this.isEdit = true;
    this.createEmployee.controls["surname"].setValue(employee.surname);
    this.createEmployee.controls["secondSurname"].setValue(
      employee.secondSurname
    );
    this.createEmployee.controls["firstName"].setValue(employee.firstName);
    this.createEmployee.controls["otherNames"].setValue(employee.otherNames);
    this.createEmployee.controls["countryEmployment"].setValue(
      employee.countryEmployment
    );
    this.createEmployee.controls["identificationType"].setValue(
      employee.identificationType
    );
    this.createEmployee.controls["identificationNumber"].setValue(
      employee.identificationNumber
    );
    this.createEmployee.controls["email"].setValue(employee.email);
    this.createEmployee.controls["admissionDate"].setValue(
      moment(employee.admissionDate).format("YYYY-MM-DD")
    );
    this.createEmployee.controls["area"].setValue(employee.area);
    this.createEmployee.controls["state"].setValue(employee.state);
    this.createEmployee.controls["registrationDate"].setValue(
      moment(employee.registrationDate).format().substring(0, 16)
    );
    if (employee.modificationDate) {
      this.createEmployee.controls["modificationDate"].setValue(
        moment(employee.modificationDate).format().substring(0, 16)
      );
    this.createEmployee.controls["modificationDate"].disable();
    }
    localStorage.removeItem("employeeToEdit");
  }

  goToCancel() {
    localStorage.clear();
    this.goToConsulta();
  }

  goToConsulta() {
    this.router.navigateByUrl("/consulta");
  }

  onSubmit() {
    this.spinner.show();
    let employee: Employee = new Employee();
    employee.surname = this.createEmployee.controls["surname"].value;
    employee.secondSurname =
      this.createEmployee.controls["secondSurname"].value;
    employee.firstName = this.createEmployee.controls["firstName"].value;
    employee.otherNames = this.createEmployee.controls["otherNames"].value;
    employee.countryEmployment =
      this.createEmployee.controls["countryEmployment"].value;
    employee.identificationType =
      this.createEmployee.controls["identificationType"].value;
    employee.identificationNumber =
      this.createEmployee.controls["identificationNumber"].value;
    employee.email = this.createEmployee.controls["email"].value;
    employee.admissionDate =
      this.createEmployee.controls["admissionDate"].value;
    employee.area = this.createEmployee.controls["area"].value;
    employee.state = this.createEmployee.controls["state"].value;
    employee.registrationDate =
      this.createEmployee.controls["registrationDate"].value;
    employee.modificationDate =
      this.createEmployee.controls["modificationDate"].value;

    let employeeId = this.employeeEdit.employeeId
      ? this.employeeEdit.employeeId
      : 0;
    if (!this.isEdit) {
      this.employeeService
        .postEmployee(employee)
        .subscribe((response: Response<Employee>) => {
          if (response.ok && response.data) {
            console.log(response);
            this.messageResponse = response.message;
            localStorage.clear();
            this.spinner.hide();
          }
        });
    } else if (employeeId) {
      this.employeeService
        .putEmployee(employeeId, employee)
        .subscribe((response: Response<Employee>) => {
          if (response.ok && response.data) {
            this.messageResponse = response.message;
            localStorage.clear();
            this.spinner.hide();
          }
        });
    }
  }
}
