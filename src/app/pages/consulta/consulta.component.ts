import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
} from "@angular/forms";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { Country } from "src/app/models/country.model";
import { Employee } from "src/app/models/employee.model";
import { IdentificationType } from "src/app/models/indetificationtype.model";
import { Response } from "src/app/models/utils/response.model";
import { EmployeeService } from "src/app/service/employee.service";

@Component({
  selector: "consulta",
  templateUrl: "./consulta.component.html",
  styleUrls: ["./consulta.component.css"],
})
export class ConsultaComponent implements OnInit {
  config: any = {
    itemsPerPage: 10,
    currentPage: 1,
    totalItems: 100
  };
  public maxSize: number = 7;
  public directionLinks: boolean = true;
  public autoHide: boolean = false;
  public responsive: boolean = true;
  public labels: any = {
      previousLabel: '<--',
      nextLabel: '-->',
      screenReaderPaginationLabel: 'Pagination',
      screenReaderPageLabel: 'page',
      screenReaderCurrentLabel: `You're on page`
  };

  constructor(
    private employeeService: EmployeeService,
    private router: Router,
    private employeeForm: FormBuilder,
    private spinner: NgxSpinnerService
  ) {}

  public messageResponse: any = "";
  public employees: Array<Employee> = [];
  public countries: Array<Country> = [];
  public identificationTypes: Array<IdentificationType> = [];

  public filterEmployee: FormGroup = this.employeeForm.group({
    surname: [""],
    secondSurname: [""],
    firstName: [""],
    otherNames: [""],
    countryEmployment: [""],
    identificationType: [""],
    identificationNumber: [""],
    email: [""],
    state: [""],
  });

  ngOnInit() {
    this.loadEmployees();
    this.chargeLists();
  }

  chargeLists() {
    this.employeeService
      .getCountries()
      .subscribe((response: Response<Country>) => {
        if (response.data && response.data.length > 0) {
          this.countries = response.data;
        }
      });

    this.employeeService
      .getIdentificationType()
      .subscribe((response: Response<IdentificationType>) => {
        if (response.data && response.data.length > 0) {
          this.identificationTypes = response.data;
        }
      });
  }

  loadEmployees() {
    this.spinner.show();
    this.employeeService
      .getEmployees({
        perPage: this.config.itemsPerPage,
        currentPage: this.config.currentPage
      })
      .subscribe((response: Response<Employee>) => {
        if (response.data && response.data.length > 0) {
          this.employees = response.data;
          this.spinner.hide();
        } else if (response.data && response.data.length == 0) {
          this.employees = [];
          this.spinner.hide();
        }
      });
  }

  filterEmployees() {
    let params = {
      currentPage: this.config.currentPage,
      perPage: this.config.itemsPerPage
    };
    Object.keys(this.filterEmployee.controls).forEach((control) => {
      if (this.filterEmployee.controls[control].value != "") {
        if (control == "identificationType" || control == "countryEmployment") {
          params = {
            [control]:
              control == "identificationType"
                ? this.filterEmployee.controls[control].value[
                    "identificationTypeId"
                  ]
                : this.filterEmployee.controls[control].value["countryId"],
            ...params,
          };
        } else {
          params = {
            [control]: this.filterEmployee.controls[control].value,
            ...params,
          };
        }
      }
    });
    if (Object.keys(params).length > 0) {
      this.employeeService
        .getEmployees(params)
        .subscribe(
          (response: Response<Employee>) => {
          if (response.data && response.data.length > 0) {
            this.employees = response.data;
          }
        }, (error: any) => {
          if (error.status == 404) {
            this.employees = [];
          }
        }
        );
    } else {
      this.employeeService
        .getEmployees()
        .subscribe((response: Response<Employee>) => {
          if (response.data && response.data.length > 0) {
            this.employees = response.data;
          }
        });
    }
  }

  goToCreate(edit: boolean = false, employee?: Employee) {
    if (edit) localStorage.setItem("employeeToEdit", JSON.stringify(employee));
    this.router.navigateByUrl("/creacion");
  }

  deleteEmployee(employeeId: any) {
    this.employeeService
      .deleteEmployee(employeeId)
      .subscribe((response: Response<Employee>) => {
        if (response.ok) {
          this.messageResponse = response.message;
          this.loadEmployees();
        }
      });
  }

  onPageChange(event: any) {
    this.config.currentPage = event;
    this.loadEmployees();
  }
}
