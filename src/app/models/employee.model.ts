import { Area } from "./area.model";
import { Country } from "./country.model";
import { IdentificationType } from "./indetificationtype.model";

export class Employee {
employeeId?: number;
surname?: string;
secondSurname?: string;
firstName?: string;
otherNames?: string;
countryEmployment?: Country;
identificationType?: IdentificationType;
identificationNumber?: string;
email?: string;
admissionDate?: Date;
area?: Area;
state?: string;
registrationDate?: Date;
modificationDate?: Date;
}